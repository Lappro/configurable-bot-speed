#!/bin/bash
if ! which jq > /dev/null; then
    echo "Please install jq first, e.g. with 'sudo apt install jq'"
    exit 1
fi

MOD_NAME="$(jq -r .name < info.json)"
CURRENT_FOLDER="$(pwd | rev | cut -d'/' -f1 | rev)"

if [ ! -f "$PWD/release.sh" ] || [ "${CURRENT_FOLDER}" != "${MOD_NAME}" ]; then
    echo "Run this script from within the project and make sure the project folder name equals the mod machine name: ${MOD_NAME}"
    exit 1
fi

VERSION="$(jq -r .version < info.json)"
ZIP_NAME="${MOD_NAME}_${VERSION}.zip"

if [ -f "$ZIP_NAME" ]; then
    echo "Release zip already exists: ${ZIP_NAME}"
    exit 1
fi

echo "Making release ${ZIP_NAME}"

cd ..
zip -r "${MOD_NAME}/${ZIP_NAME}" $MOD_NAME/*.{lua,json,png,md,txt} $MOD_NAME/{graphics,locale,migrations,prototypes,scripts,stdlib}
